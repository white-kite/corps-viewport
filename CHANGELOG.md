# Changelog


## v1.4.0...v1.4.1


### 🏡 框架

  - Add README.md (ccc27d9)
  - Generate CHANGELOG.md (5316031)
  - **viewport:** 修改sideEffects 配置 (19f3a92)
  - Release v1.4.1 (ce68b93)

### ❤️  Contributors

- Whitekite

## v1.3.2...v1.4.0

### 💅 重构

- 基于 monorepo 重构项目;
- 优化过于不成熟的 esmodule 用法;
- 取消 css module用法,使样式更容易覆盖;
- 删除 lodash-es依赖，避免包在cjs环境无法执行;

### 🏡 框架

  - Release v1.4.0 (c2fcbf2)

### ⚠️ 破坏性改动

- 基于 monorepo 重构项目;
- 优化过于不成熟的 esmodule 用法;
- 取消 css module用法,使样式更容易覆盖;
- 删除 lodash-es依赖，避免包在cjs环境无法执行;

### ❤️  Contributors

- Whitekite

## v1.4.0...v1.4.0

