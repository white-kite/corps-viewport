import { createApp } from 'vue'
import App from './App'
import './index.scss'

createApp(App).mount('#app')
