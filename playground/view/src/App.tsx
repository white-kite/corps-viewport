import { defineComponent, ref } from 'vue'
import {
  useCornerstoneChangeDicomFileUrl,
  useCornerstoneRegister,
  useNormalCornerstoneToolList,
  CornerstoneViewport,
} from '@whitekite/corps-viewport'

/**
 * SPA主入口
 */
const App = defineComponent({
  name: 'App',
  setup() {
    useCornerstoneRegister()
    const { changeUrlList } = useCornerstoneChangeDicomFileUrl()
    const toolList = useNormalCornerstoneToolList()
    const syncElement = ref<HTMLElement>()
    const activeTool = ref<string | undefined>('Pan')
    return () => (
      <section style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
        <header>
          <button
            onClick={() => {
              if (activeTool.value) {
                activeTool.value = undefined
              } else {
                activeTool.value = 'GridDividingLine'
              }
            }}
          >
            scrollTOIndex
          </button>
        </header>
        <main style={{ height: '100%', flex: '1 1 auto' }}>
          <CornerstoneViewport
            hideSensitiveInformation
            // 使用 https://github.com/cornerstonejs/react-cornerstone-viewport/tree/master/examples 示例dicom片
            imageIds={changeUrlList([
              'https://s3.amazonaws.com/lury/PTCTStudy/1.3.6.1.4.1.25403.52237031786.3872.20100510032220.11.dcm',
            ])}
            tools={toolList.value}
            activeTool={activeTool.value}
            onViewportInit={(element) => {
              if (element) {
                syncElement.value = element
              }
            }}
          />
        </main>
      </section>
    )
  },
})
export default App
