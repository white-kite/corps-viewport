# corps-viewport

使用基石封装的vue3-dicom查看器组件

## 安装

```
yarn

yarn add @whitekite/corps-viewport

npm

npm i @whitekite/corps-viewport

pnpm

pnpm add @whitekite/corps-viewport
```

## CornerstoneViewport

主要的显示组件
<table>
<tr>
<th>字段</th>
<th>说明</th>
<th>类型</th>
</tr>
<tr>
<td colspan="3">
<strong>API</strong>
</td>
</tr>
<tr>
  <td>activeTool</td>
  <td>当前激活的工具</td>
  <td>string</td>
</tr>
<tr>
  <td>synchronizationContext</td>
  <td>同步工具</td>
  <td><a href="https://tools.cornerstonejs.org/examples/tools/wwwc-sync.html">Sync</a></td>
</tr>
<tr>
  <td>tools</td>
  <td>使用的工具集：注册才可使用</td>
  <td>Tool</td>
</tr>
<tr>
  <td>imageIds</td>
  <td>加载的dicom片：请使用changedicom工具将普通url转为dicomweb标准</td>
  <td>string[]</td>
</tr>
<tr>
  <td>style</td>
  <td>自定义样式</td>
  <td>CSSProperties</td>
</tr>
<tr>
  <td>class</td>
  <td>自定义样式class</td>
  <td>string</td>
</tr>
<tr>
  <td>style</td>
  <td>自定义样式</td>
  <td>CSSProperties</td>
</tr>
<tr>
<td colspan="3">
<strong>Event</strong>
</td>
</tr>
<tr>
  <td>onSetViewportActive</td>
  <td>当查看器被手势激活时</td>
  <td>VoidFunction</td>
</tr>
<tr>
  <td>onViewportInit</td>
  <td>当查看器被初始化</td>
  <td>(element: HTMLElement | null) => void</td>
</tr>
<tr>
  <td>onImageLoad</td>
  <td>当dicom片加载完成</td>
  <td>(element: HTMLElement | null) => void</td>
</tr>
<tr>
<td colspan="3">
<strong>Tool</strong>
<strong><a href="https://tools.cornerstonejs.org/examples">示例以及常见工具</a></strong>
<strong><a href="https://github.com/cornerstonejs/cornerstoneTools/blob/master/src/index.js">所有工具</a></strong>
</td>
</tr>
<tr>
<td>name</td>
<td>工具的名称</td>
<td>?string<</td>
</tr>
<tr>
<td>name</td>
<td>工具的名称</td>
<td>?string</td>
</tr>
<tr>
<td>toolClass</td>
<td>自定义工具:内部自带工具GridDividingLineTool详见下方表格</td>
<td>BaseTool extends Class</td>
</tr>
<tr>
<td>props</td>
<td>工具的配置:<a href="https://github.com/cornerstonejs/cornerstoneTools/blob/master/src/store/addTool.js">详细配置</a></td>
<td>Record<&nbsp;string&nbsp;,&nbsp;string&nbsp;>&nbsp; </td>
</tr>
<tr>
<td>mode</td>
<td>工具模式</td>
<td>active | passive | enabled | disabled</td>
</tr>
<tr>
<td>modeOptions</td>
<td>工具的配置:<a href="https://github.com/cornerstonejs/cornerstoneTools/blob/master/src/store/setToolMode.js">详细配置</a></td>
<td>{ mouseButtonMask: number } | Record<&nbsp;string, number&nbsp;></td>
</tr>
<tr>
</table>

## 样式

```tsx
import '@whitekite/corps-viewport/dist/style.css'
// or 
import '@whitekite/corps-viewport/style'
```

## Example

```tsx
<CornerstoneViewport
  imageIds={item}
  onViewportInit={(element) => {
    if (element) {
      // 元素是否存在，存在则将同步元素更新
      syncElement.value[index] = element
    }
  }}
  tools={tools.value}
  activeTool={activeTool.value}
  synchronizationContext={syncTool.value}
  onImageLoad={(load) => {
    imageLoadList.value[index] = load
  }}
  onSetViewportActive={() => {
    activeKey.value = index
  }}
/>
```

# Util·工具

### usePanZoomSynchronizer

移动

```tsx
/**
 * synchronizationContext的一种
 * @return new Synchronizer('cornerstoneimagerendered', panZoomSynchronizer)
 */
const panZoomSynchronizer = usePanZoomSynchronizer()
```

### useWwwcSynchronizer

调窗同步

```tsx
/**
 * synchronizationContext的一种
 * @return new Synchronizer('cornerstoneimagerendered', wwwcSynchronizer)
 */
const wwwcSynchronizer = useWwwcSynchronizer()
```

### GridDividingLineTool

自定义胸肺比工具

```tsx
tools.push({
  name: 'GridDividingLine',
  toolClass: GridDividingLineTool,
  mode: 'disabled',
  modeOptions: {mouseButtonMask: 1},
})
```

### useCornerstoneChangeDicomFileUrl

将普通下载url转为dicomweb标准url

```tsx
/**
 * 改变url使基石可以直接读dicom文件
 */
const {changeUrlList} = useCornerstoneChangeDicomFileUrl()

imageIds.value[0] = changeUrlList(urlList)
```

### useCornerstoneRegister

注册dicom查看器工具 <span style="color:#f00">(必须)</span>

```tsx
/**
 * 注册基石
 */
useCornerstoneRegister()
```

### useNormalCornerstoneToolList

内置的Cornerstone常用工具数组，可用于便捷注册工具数组

> ### 内置工具：
>
> Wwwc
>
> Pan
>
> Zoom
>
> Magnify
>
> Rotate
>
> CobbAngle
>
> Length
>
> Bidirectional
>
> Angle
>
> RectangleRoi
>
> Probe
>
> EllipticalRoi
>
> FreehandRoi

```tsx
const toolList = useNormalCornerstoneToolList();

<CornerstoneViewport
  imageIds={changeUrlList([])}
  tools={toolList.value}
  activeTool="GridDividingLine"
/>
```

### useCornerstoneResetElements

重置dicom查看器

注意：使用钢笔工具请勿调用此方法

<span style="color:#f00">对绘图工具的清空本质上是直接清空工具状态</span>

<span style="color:#f00">暂时只能清空工具内写死的绘图工具状态，一键清空，请期待后续更新</span>

<span style="color:#f00">如封装额外绘图工具请自行封装该方法</span>

> ### 支持清空绘图状态：
>
>  FreehandRoi
>
> GridDividingLine
>
> CobbAngle
>
> Length
>
> Bidirectional
>
> Angle
>
> RectangleRoi
>
> Probe
>
> EllipticalRoi
>

```tsx
 /**
 * 重置
 * 注意：使用钢笔工具请勿调用此方法
 */
const cornerstoneResetElements = useCornerstoneResetElements(syncElement)
```

### useCornerstoneScreenSizeCalibration

根据屏幕的真实宽度（单位：mm）计算影像真实尺寸显示大小

```tsx

const calibrateThroughPicture = useCornerstoneScreenSizeCalibration();
/**
 * calibrateThroughPicture(element:读片器的元素，屏幕的真实宽度：mm)
 */
<button onClick={() => calibrateThroughPicture(syncElement.value, 500)}>自适应</button>
```

### useCornerstoneScrollToIndex

指定滚动至序列下标

```tsx
const scrollToIndex = useCornerstoneScrollToIndex();

<button onClick={() => scrollToIndex(syncElement.value, 1)}>滚动至指定下标</button>
```

### useCornerstonePlayClip

播放序列片

```tsx
const {
  // 播放状态
  state,
  // 播放状态
  clipState,
  // 播放
  play,
  // 暂停
  stop,
  // 切换播放状态
  toggle,
  // 播放
  playClip,
  // 暂停
  stopClip,
  // 切换播放状态
  toggleClip,
} = useCornerstonePlayClip();

/**
 * 播放
 * @param element 读片器所在元素
 * @param frame 播放帧数
 */
playClip(syncElement.value, 24)

/**
 * 暂停
 * @param element 读片器所在元素
 */
stopClip(syncElement.value)

/**
 * 切换播放状态
 * @param element 读片器所在元素
 * @param frame 播放帧数
 */
toggleClip(syncElement.value, 24)
```
