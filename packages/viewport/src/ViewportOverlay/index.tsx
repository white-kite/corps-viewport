import { Fragment, FunctionalComponent } from 'vue'
import { formatPN, formatDA, formatNumberPrecision, formatTM, isValidNumber } from '../helpers'
import cornerstone from 'cornerstone-core'
import styles from './index.module.scss'

const getCompression = (imageId: string) => {
  const generalImageModule = cornerstone.metaData.get('generalImageModule', imageId) || {}
  const { lossyImageCompression, lossyImageCompressionRatio, lossyImageCompressionMethod } =
    generalImageModule

  if (lossyImageCompression === '01' && lossyImageCompressionRatio !== '') {
    const compressionMethod = lossyImageCompressionMethod || 'Lossy: '
    const compressionRatio = formatNumberPrecision(lossyImageCompressionRatio, 2)
    return compressionMethod + compressionRatio + ' : 1'
  }

  return 'Lossless / Uncompressed'
}

export interface ViewportOverlayProps {
  scale: number
  windowWidth: number | string
  windowCenter: number | string
  imageId: string
  imageIndex: number
  stackSize: number
  hideSensitiveInformation: boolean
}

export type ViewportOverlayType = FunctionalComponent<ViewportOverlayProps>

export const ViewportOverlay: ViewportOverlayType = ({
  imageId,
  scale,
  windowWidth,
  windowCenter,
  imageIndex,
  stackSize,
  hideSensitiveInformation,
}) => {
  if (!imageId) {
    return null
  }
  /**
   * 获取缩放值
   */
  const zoomPercentage = formatNumberPrecision(scale * 100, 0)
  const seriesMetadata = cornerstone.metaData.get('generalSeriesModule', imageId) || {}
  const imagePlaneModule = cornerstone.metaData.get('imagePlaneModule', imageId) || {}
  const { rows, columns, sliceThickness, sliceLocation } = imagePlaneModule
  const { seriesNumber, seriesDescription } = seriesMetadata

  const generalStudyModule = cornerstone.metaData.get('generalStudyModule', imageId) || {}
  const { studyDate, studyTime, studyDescription } = generalStudyModule

  const patientModule = cornerstone.metaData.get('patientModule', imageId) || {}
  const { patientId, patientName } = patientModule

  const generalImageModule = cornerstone.metaData.get('generalImageModule', imageId) || {}
  const { instanceNumber } = generalImageModule

  const cineModule = cornerstone.metaData.get('cineModule', imageId) || {}

  const { frameTime } = cineModule

  const frameRate = formatNumberPrecision(1000 / frameTime, 1)
  const compression = getCompression(imageId)
  const wwwc = `W: ${
    typeof windowWidth !== 'string' && windowWidth.toFixed ? windowWidth.toFixed(0) : windowWidth
  } L: ${
    // @ts-ignore
    typeof windowWidth !== 'string' && windowWidth.toFixed
      ? typeof windowCenter !== 'string'
        ? windowCenter.toFixed(0)
        : windowCenter
      : windowCenter
  }`
  const imageDimensions = isValidNumber(columns) && isValidNumber(rows) && `${columns} x ${rows}`

  const normal = (
    <Fragment>
      <div class={[styles.topLeft, styles.overlayElement]}>
        {hideSensitiveInformation || <div>{formatPN(patientName)}</div>}
        <div>{patientId}</div>
      </div>
      <div class={[styles.topRight, styles.overlayElement]}>
        <div>{studyDescription}</div>
        {hideSensitiveInformation || (
          <div>
            {formatDA(studyDate)} {formatTM(studyTime)}
          </div>
        )}
      </div>
      <div class={[styles.bottomRight, styles.overlayElement]}>
        <div>Zoom: {zoomPercentage}%</div>
        <div>{wwwc}</div>
        <div class={styles.compressionIndicator}>{compression}</div>
      </div>
      <div class={[styles.bottomLeft, styles.overlayElement]}>
        <div>{seriesNumber >= 0 ? `Ser: ${seriesNumber}` : ''}</div>
        <div>{stackSize > 1 ? `Img: ${instanceNumber || ''} ${imageIndex}/${stackSize}` : ''}</div>
        <div>
          {frameRate && frameRate >= 0 ? `${formatNumberPrecision(frameRate, 2)} FPS` : ''}
          <div>{imageDimensions}</div>
          <div>
            {isValidNumber(sliceLocation)
              ? `Loc: ${formatNumberPrecision(sliceLocation, 2)} mm `
              : ''}
            {sliceThickness ? `Thick: ${formatNumberPrecision(sliceThickness, 2)} mm` : ''}
          </div>
          <div>{seriesDescription}</div>
        </div>
      </div>
    </Fragment>
  )

  return <div class={styles.viewportOverlay}>{normal}</div>
}
