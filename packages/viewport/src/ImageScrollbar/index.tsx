import { FunctionalComponent } from 'vue'
import styles from './index.module.scss'
interface ImageScrollbarProps {
  value: number
  max: number
  height: string
  onInputCallback: (value: number) => void
}

type ImageScrollbarType = FunctionalComponent<ImageScrollbarProps>

/**
 * 图片滚动条
 * @param props
 * @constructor
 * @copyright https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/ImageScrollbar/ImageScrollbar.js
 */
export const ImageScrollbar: ImageScrollbarType = (props) => {
  if (props.max === 0) {
    return null
  }
  const style = {
    width: `${props.height}`,
  }

  const onChange = (event: { target: { value: string } }) => {
    const intValue = parseInt(event?.target?.value, 10)
    props.onInputCallback(intValue)
  }

  const onKeyDown = (event: InputEvent) => {
    const keys = {
      DOWN: 40,
      UP: 38,
    }
    if (event.which === keys.DOWN) {
      event.preventDefault()
    } else if (event.which === keys.UP) {
      event.preventDefault()
    }
  }

  return (
    <div class={styles.scroll}>
      <div class={styles.scrollHolder}>
        <input
          class={styles.imageSlider}
          style={style}
          type="range"
          min="0"
          max={props.max}
          step="1"
          value={props.value}
          // @ts-ignore
          onChange={onChange}
          onKeyDown={onKeyDown}
        />
      </div>
    </div>
  )
}
