import cornerstoneTools from 'cornerstone-tools'
import styles from './index.module.scss'
import { FunctionalComponent } from 'vue'

const getOrientationMarkers = (
  rowCosines: string[],
  columnCosines: string[],
  rotationDegrees: number,
  isFlippedVertically: boolean,
  isFlippedHorizontally: boolean
) => {
  const { getOrientationString, invertOrientationString } = cornerstoneTools.orientation
  const rowString = getOrientationString(rowCosines)
  const columnString = getOrientationString(columnCosines)
  const oppositeRowString = invertOrientationString(rowString)
  const oppositeColumnString = invertOrientationString(columnString)

  const markers = {
    top: oppositeColumnString,
    left: oppositeRowString,
  }

  // If any vertical or horizontal flips are applied, change the orientation strings ahead of
  // the rotation applications
  if (isFlippedVertically) {
    markers.top = invertOrientationString(markers.top)
  }

  if (isFlippedHorizontally) {
    markers.left = invertOrientationString(markers.left)
  }

  // Swap the labels accordingly if the viewport has been rotated
  // This could be done in a more complex way for intermediate rotation values (e.g. 45 degrees)
  if (rotationDegrees === 90 || rotationDegrees === -270) {
    return {
      top: markers.left,
      left: invertOrientationString(markers.top),
    }
  } else if (rotationDegrees === -90 || rotationDegrees === 270) {
    return {
      top: invertOrientationString(markers.left),
      left: markers.top,
    }
  } else if (rotationDegrees === 180 || rotationDegrees === -180) {
    return {
      top: invertOrientationString(markers.top),
      left: invertOrientationString(markers.left),
    }
  }

  return markers
}

interface ViewportOrientationMarkersProps {
  rowCosines: string[]
  columnCosines: string[]
  rotationDegrees: number
  isFlippedVertically: boolean
  isFlippedHorizontally: boolean
}

type ViewportOrientationMarkersType = FunctionalComponent<ViewportOrientationMarkersProps>

/**
 * ViewportOrientationMarkers
 * @param rowCosines
 * @param columnCosines
 * @param rotationDegrees
 * @param isFlippedVertically
 * @param isFlippedHorizontally
 * @constructor
 * @copyright https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/ViewportOrientationMarkers/ViewportOrientationMarkers.js
 */
export const ViewportOrientationMarkers: ViewportOrientationMarkersType = ({
  rowCosines,
  columnCosines,
  rotationDegrees,
  isFlippedVertically,
  isFlippedHorizontally,
}) => {
  if (!rowCosines || !columnCosines) {
    return ''
  }

  const markers = getOrientationMarkers(
    rowCosines,
    columnCosines,
    rotationDegrees,
    isFlippedVertically,
    isFlippedHorizontally
  )

  return (
    <div class={styles.viewportOrientationMarkers}>
      <div class={[styles.topMid, styles.orientationMarker]}>{markers.top}</div>
      <div class={[styles.leftMid, styles.orientationMarker]}>{markers.left}</div>
    </div>
  )
}
