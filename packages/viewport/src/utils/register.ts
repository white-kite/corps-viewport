import cornerstone from 'cornerstone-core'
import Hammer from 'hammerjs'
import dicomParser from 'dicom-parser'
// @ts-ignore
import cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader'
// @ts-ignore
import cornerstoneTools from 'cornerstone-tools'

// @ts-ignore
import cornerstoneMath from 'cornerstone-math'

export const useCornerstoneRegister = (webWorkerTaskPaths?: any[]) => {
  cornerstoneTools.external.cornerstone = cornerstone
  cornerstoneTools.external.Hammer = Hammer
  cornerstoneTools.external.cornerstoneMath = cornerstoneMath
  cornerstoneTools.toolStyle.setToolWidth(2)
  cornerstoneTools.toolColors.setToolColor('#3f6212')
  cornerstoneTools.toolColors.setActiveColor('#365314')
  cornerstoneTools.init({
    showSVGCursors: true,
    globalToolSyncEnabled: true,
  })
  cornerstoneWADOImageLoader.external.dicomParser = dicomParser
  cornerstoneWADOImageLoader.external.cornerstone = cornerstone
  // cornerstoneWebImageLoader.external.cornerstone = cornerstone
  try {
    cornerstoneWADOImageLoader.webWorkerManager.initialize({
      maxWebWorkers: navigator.hardwareConcurrency || 1,
      startWebWorkersOnDemand: true,
      webWorkerTaskPaths,
      taskConfiguration: {
        decodeTask: {
          initializeCodecsOnStartup: false,
          strict: true,
        },
      },
    })
  } catch (e) {
    console.warn(e)
  }
}
