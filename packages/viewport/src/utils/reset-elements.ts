import { Ref } from 'vue'
import { useCornerstoneReset } from './reset'
import { isArrayEmpty } from '@vill-v/vanilla'

/**
 * 基石查看器批量重置
 */
export const useCornerstoneResetElements = (elements: Ref<HTMLElement[]>) => {
  const resetElement = useCornerstoneReset()
  return () => {
    if (!elements.value || isArrayEmpty(elements.value)) {
      return
    }
    elements.value.forEach((element) => {
      resetElement(element)
    })
  }
}
