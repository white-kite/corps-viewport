import { EVENTS as CORE_EVENTS, events } from 'cornerstone-core'
import { EVENTS } from 'cornerstone-tools'
import { Ref, ref, unref } from 'vue'
import { useEventListener, MaybeRef } from '@vueuse/core'
import { stackPrefetch } from 'cornerstone-tools'

export const useCornerstoneStackPrefetch = (
  element: MaybeRef<HTMLElement | undefined>,
  error: Ref<{ message: string } | null>
) => {
  const isPrefetchLoading = ref(false)

  useEventListener(element, EVENTS.STACK_PREFETCH_DONE, () => {
    stackPrefetch.disable(unref(element))
    isPrefetchLoading.value = false
  })

  const saveErrorImageIndex = () => {
    if (unref(isPrefetchLoading)) {
      isPrefetchLoading.value = false
      error.value = { message: '预加载失败，请检查序列影像' }
    }
  }

  useEventListener(events, CORE_EVENTS.IMAGE_LOAD_FAILED, saveErrorImageIndex)

  return isPrefetchLoading
}
