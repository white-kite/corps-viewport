// @ts-ignore
import { resize } from 'cornerstone-core'

/**
 * 基石重新布局
 */
export const useCornerstoneResize = () => {
  return (element?: HTMLElement, forceFitToWindow = true) => {
    if (element) {
      resize(element, forceFitToWindow)
    }
  }
}
