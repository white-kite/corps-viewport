import { playClip, stopClip } from 'cornerstone-tools'

import { ref, unref } from 'vue'
import { useToggle, MaybeRef } from '@vueuse/core'

/**
 * 播放工具
 */
export const useCornerstonePlayClip = () => {
  const [state, toggleState] = useToggle(false)

  const playFramesPerSecond = ref(24)
  const stop = (element: MaybeRef<HTMLElement | undefined>) => {
    const ele = unref(element)
    if (ele) {
      stopClip(ele)
    }
  }

  const play = (element: MaybeRef<HTMLElement | undefined>, framesPerSecond: number) => {
    const ele = unref(element)
    if (ele) {
      stopClip(ele)
      playClip(ele, framesPerSecond)
    }
  }

  const toggle = (element: MaybeRef<HTMLElement | undefined>, framesPerSecond: number) => {
    toggleState()
    if (state.value) {
      play(element, framesPerSecond)
    } else {
      stop(element)
    }
  }

  return {
    state,
    clipState: state,
    play,
    stop,
    toggle,
    playClip: play,
    stopClip: stop,
    toggleClip: toggle,
    playFramesPerSecond,
  }
}
