/**
 * 更改dicom文件为基石可读取文件
 */

export const useCornerstoneChangeDicomFileUrl = () => {
  /**
   * 更改单个url
   * @param url
   */
  const changeUrl = (url: string) => 'dicomweb:' + url
  /**
   * 更改url数组
   * @param list
   */
  const changeUrlList = (list: Array<string>) => list.map((item) => changeUrl(item))

  return { changeUrl, changeUrlList }
}
