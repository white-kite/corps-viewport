import { ref } from 'vue'
import {
  enable as cornerstoneEnable,
  disable as cornerstoneDisable,
  // @ts-ignore
} from 'cornerstone-core'

// @ts-ignore
import { stackPrefetch, clearToolState } from 'cornerstone-tools'

/**
 * 基石启用与禁用
 */
export const useCornerstone = () => {
  const viewRef = ref<HTMLElement>()
  /**
   * 启用
   * @param element
   */
  const enable = (element?: HTMLElement) => {
    if (element) {
      cornerstoneEnable(element, { renderer: 'webgl' })
    }
  }

  /**
   * 禁用
   * @param element
   */
  const disable = (element?: HTMLElement) => {
    if (element) {
      stackPrefetch.disable(element)
      clearToolState(element, 'stackPrefetch')
      cornerstoneDisable(element)
    }
  }

  return { viewRef, enable, disable }
}
