import {
  displayImage as displayImageCore,
  events,
  EVENTS,
  loadAndCacheImage as loadAndCacheImageCore,
  // @ts-ignore
} from 'cornerstone-core'
// @ts-ignore
import { EVENTS as TOOL_EVENTS } from 'cornerstone-tools'

import { Ref, ref } from 'vue'

/**
 * 基石加载图片
 */
export const useCornerstoneLoadImage = (
  imageIds: Ref<Array<string>>,
  emit: (event: 'setViewportActive') => void
) => {
  /**
   * 图片是否正在加载
   */
  const isImageLoading = ref(false)

  const error = ref<{ message: string } | null>(null)
  /**
   * 图片当前显示下标
   */
  const imageIdIndex = ref<number>(0)
  const scale = ref(0)
  const windowCenter = ref(0)
  const windowWidth = ref(0)
  const rotationDegrees = ref(0)
  const isFlippedVertically = ref(false)
  const isFlippedHorizontally = ref(false)
  /**
   * 加载并缓存图片
   * @param imageId
   */
  const loadAndCacheImage = (imageId: string): Promise<unknown> => {
    isImageLoading.value = true
    error.value = null
    return (loadAndCacheImageCore(imageId) as Promise<unknown>)
      .catch((err) => {
        error.value = err
      })
      .finally(() => (isImageLoading.value = false))
  }
  /**
   * 显示图片
   * @param element
   * @param image
   */
  const displayImage = (element: HTMLElement | undefined, image: unknown) => {
    if (element) {
      displayImageCore(element, image)
    }
  }

  /**
   * 图片加载进度
   */
  const imageProgress = ref(0)

  /**
   * 图片加载进度
   * @param e
   */
  const onImageProgress = (e: { detail: { percentComplete: number } }) => {
    imageProgress.value = e.detail.percentComplete
  }

  /**
   * 基石监听器
   * @param clear
   */
  const bindInternalCornerstoneEventListeners = (clear = false) => {
    const addOrRemoveEventListener = clear ? 'removeEventListener' : 'addEventListener'

    /**
     * 监听图片加载速度
     */
    events[addOrRemoveEventListener]('cornerstoneimageloadprogress', onImageProgress)
  }
  /**
   * 图像渲染
   * @param event
   */
  const onImageRendered = (event: never) => {
    // @ts-ignore
    const viewport = event.detail.viewport

    scale.value = viewport.scale
    windowCenter.value = viewport.voi.windowCenter
    windowWidth.value = viewport.voi.windowWidth
    rotationDegrees.value = viewport.rotation
    isFlippedVertically.value = viewport.vflip
    isFlippedHorizontally.value = viewport.hflip
  }

  /**
   * 新图片处理器
   * @param event
   */
  const onNewImageHandler = (event: { detail: { image: { imageId: string } } }) => {
    const { imageId } = event.detail.image
    imageIdIndex.value = imageIds.value.indexOf(imageId)
  }
  /**
   * 新图片
   * @param event
   */
  const onNewImage = (event: { detail: { image: { imageId: string } } }) => onNewImageHandler(event)

  const setViewportActive = () => {
    emit('setViewportActive')
  }

  const bindInternalElementEventListeners = (element: HTMLElement | undefined, clear = false) => {
    const addOrRemoveEventListener = clear ? 'removeEventListener' : 'addEventListener'
    // Updates state's imageId, and imageIndex

    // @ts-ignore
    element?.[addOrRemoveEventListener](EVENTS.NEW_IMAGE, onNewImage)

    element?.[addOrRemoveEventListener](
      EVENTS.IMAGE_RENDERED,

      // @ts-ignore
      onImageRendered
    )
    // Set Viewport Active
    element?.[addOrRemoveEventListener](TOOL_EVENTS.MOUSE_CLICK, setViewportActive)
    element?.[addOrRemoveEventListener](TOOL_EVENTS.MOUSE_DOWN, setViewportActive)
    element?.[addOrRemoveEventListener](TOOL_EVENTS.TOUCH_PRESS, setViewportActive)
    element?.[addOrRemoveEventListener](TOOL_EVENTS.TOUCH_START, setViewportActive)
    element?.[addOrRemoveEventListener](TOOL_EVENTS.STACK_SCROLL, setViewportActive)
  }

  return {
    isImageLoading,
    error,
    loadAndCacheImage,
    displayImage,
    bindInternalCornerstoneEventListeners,
    bindInternalElementEventListeners,
    imageProgress,
    imageIdIndex,
    scale,
    windowCenter,
    windowWidth,
    rotationDegrees,
    isFlippedVertically,
    isFlippedHorizontally,
  }
}
