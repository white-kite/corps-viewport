import { reset } from 'cornerstone-core'
import {
  clearToolState,
  // @ts-ignore
} from 'cornerstone-tools'

/**
 * 基石查看器重置
 */

export const useCornerstoneReset = () => {
  return (element?: HTMLElement) => {
    reset(element)
    clearToolState(element, 'FreehandRoi')
    clearToolState(element, 'GridDividingLine')
    clearToolState(element, 'CobbAngle')
    clearToolState(element, 'Length')
    clearToolState(element, 'Bidirectional')
    clearToolState(element, 'Angle')
    clearToolState(element, 'RectangleRoi')
    clearToolState(element, 'Probe')
    clearToolState(element, 'EllipticalRoi')
  }
}
