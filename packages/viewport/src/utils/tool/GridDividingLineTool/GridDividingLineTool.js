import { importInternal } from 'cornerstone-tools'
import calculateLongestAndShortestDiameters from './gridDividingLineToolConfig/calculateLongestAndShortestDiameters'
import addNewMeasurement from './gridDividingLineToolConfig/addNewMeasurement'
import createNewMeasurement from './gridDividingLineToolConfig/createNewMeasurement'
import pointNearTool from './gridDividingLineToolConfig/pointNearTool'
import renderToolData from './gridDividingLineToolConfig/renderToolData'
import { gridDividingCursor } from '../../../cursor'
const BaseAnnotationTool = importInternal('base/BaseAnnotationTool')
const throttle = importInternal('util/throttle')
const getPixelSpacing = importInternal('util/getPixelSpacing')
const emptyLocationCallback = (measurementData, eventData, doneCallback) => doneCallback()

/**
 * @public
 * @class LengthTool
 * @memberof Tools.Annotation
 * @classdesc Tool for measuring distances.
 * @extends Tools.Base.BaseAnnotationTool
 */
export default class GridDividingLineTool extends BaseAnnotationTool {
  constructor(props) {
    const defaultProps = {
      name: 'GridDividingLine',
      supportedInteractionTypes: ['Mouse', 'Touch'],
      svgCursor: gridDividingCursor,
      configuration: {
        changeMeasurementLocationCallback: emptyLocationCallback,
        getMeasurementLocationCallback: emptyLocationCallback,
        drawHandles: true,
        drawHandlesOnHover: true,
        hideHandlesIfMoving: false,
        renderDashed: false,
        additionalData: [],
      },
    }

    super(props, defaultProps)
    this.throttledUpdateCachedStats = throttle(this.updateCachedStats, 100)
    this.createNewMeasurement = createNewMeasurement.bind(this)
    this.pointNearTool = pointNearTool.bind(this)
    this.renderToolData = renderToolData.bind(this)
    this.addNewMeasurement = addNewMeasurement.bind(this)
  }

  updateCachedStats(image, element, data) {
    if (data.toolName !== this.name) {
      return
    }

    const pixelSpacing = getPixelSpacing(image)
    const { longestDiameter, shortestDiameter } = calculateLongestAndShortestDiameters(
      data,
      pixelSpacing
    )

    data.longestDiameter = longestDiameter
    data.shortestDiameter = shortestDiameter
    data.invalidated = false
  }
}
