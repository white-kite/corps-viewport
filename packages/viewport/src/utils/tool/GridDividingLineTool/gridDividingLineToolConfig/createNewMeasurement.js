import { importInternal } from 'cornerstone-tools'

const getLogger = importInternal('util/getLogger')

const logger = getLogger('tools:annotation:TestTool')
const getHandle = (x, y, index, extraAttributes = {}) =>
  Object.assign(
    {
      x,
      y,
      index,
      drawnIndependently: false,
      allowedOutsideImage: false,
      highlight: true,
      active: false,
    },
    extraAttributes
  )
export default function (eventData) {
  const goodEventData = eventData && eventData.currentPoints && eventData.currentPoints.image

  if (!goodEventData) {
    logger.error(`required eventData not supplied to tool ${this.name}'s createNewMeasurement`)

    return
  }

  const { x, y } = eventData.currentPoints.image

  return {
    visible: true,
    active: true,
    color: undefined,
    invalidated: true,
    isCreating: true,
    handles: {
      lastPerpendicularStart: getHandle(x, y, 4),
      lastPerpendicularEnd: getHandle(x, y, 5),
      perpendicularStart: getHandle(x, y, 2),
      perpendicularEnd: getHandle(x, y, 3),
      start: getHandle(x, y, 0),
      end: getHandle(x, y, 1, { hasMoved: true }),
    },
    longestDiameter: 0,
    shortestDiameter: 0,
  }
}
