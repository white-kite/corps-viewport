import getLineVector from './getLineVector'

export default function updatePerpendicularLineHandles(eventData, measurementData) {
  let oneThirdStartX, oneThirdStartY, oneThirdEndX, oneThirdEndY, twoThirdStartY, twoThirdEndY

  if (measurementData.handles.start.hasMoved) {
    measurementData.handles.start.x = measurementData.handles.end.x
  }
  if (measurementData.handles.end.hasMoved) {
    measurementData.handles.end.x = measurementData.handles.start.x
  }

  const {
    start,
    end,
    // perpendicularStart,
    // perpendicularEnd,
    // lastPerpendicularStart,
    // lastPerpendicularEnd,
  } = measurementData.handles

  const { columnPixelSpacing = 1, rowPixelSpacing = 1, width, height } = eventData.image

  // start.x = width / 2
  // end.x = width / 2

  if (start.y < 0) {
    measurementData.handles.start.y = 0
  }
  if (start.y > height) {
    measurementData.handles.start.y = height
  }
  if (end.y < 0) {
    measurementData.handles.end.y = 0
  }
  if (end.y > height) {
    measurementData.handles.end.y = height
  }
  if (start.x < 0) {
    measurementData.handles.start.x = 0
  }
  if (start.x > width) {
    measurementData.handles.start.x = width
  }
  if (end.x < 0) {
    measurementData.handles.end.x = 0
  }
  if (end.x > width) {
    measurementData.handles.end.x = width
  }

  if (start.x === end.x && start.y === end.y) {
    oneThirdStartX = start.x
    oneThirdStartY = start.y
    oneThirdEndX = end.x
    oneThirdEndY = end.y
    twoThirdStartY = start.y
    twoThirdEndY = end.y
  } else {
    const oneThird = {
      y: (end.y - start.y) / 3 + start.y,
    }

    const twoThird = {
      y: (end.y - start.y) * (2 / 3) + start.y,
    }

    const vector = getLineVector(columnPixelSpacing, rowPixelSpacing, start, end)

    const perpendicularLineLength = vector.length / 2
    const rowMultiplier = perpendicularLineLength / (2 * rowPixelSpacing)
    oneThirdStartX = 0
    oneThirdStartY = oneThird.y - rowMultiplier * vector.x
    oneThirdEndX = width
    oneThirdEndY = oneThird.y + rowMultiplier * vector.x
    twoThirdStartY = twoThird.y - rowMultiplier * vector.x
    twoThirdEndY = twoThird.y + rowMultiplier * vector.x
    // if (perpendicularStart.hasMoved) {
    //   const hasMovedStartX = perpendicularStart.x < 0 ? 0 : perpendicularStart.x
    //   oneThirdStartX = hasMovedStartX
    //   oneThirdEndX = width - hasMovedStartX
    // }
    // if (perpendicularEnd.hasMoved) {
    //   const hasMovedEndX = perpendicularEnd.x > width ? width : perpendicularEnd.x
    //   oneThirdEndX = hasMovedEndX
    //   oneThirdStartX = width - hasMovedEndX
    // }
    // if (lastPerpendicularStart.hasMoved) {
    //   const hasLastMovedStartX = lastPerpendicularStart.x < 0 ? 0 : lastPerpendicularStart.x
    //   oneThirdStartX = hasLastMovedStartX
    //   oneThirdEndX = width - hasLastMovedStartX
    // }
    // if (lastPerpendicularEnd.hasMoved) {
    //   const hasLastMovedEndX = lastPerpendicularEnd.x > width ? width : lastPerpendicularEnd.x
    //   oneThirdEndX = hasLastMovedEndX
    //   oneThirdStartX = width - hasLastMovedEndX
    // }
  }
  measurementData.handles.perpendicularStart.x = oneThirdStartX
  measurementData.handles.perpendicularStart.y = oneThirdStartY
  measurementData.handles.perpendicularEnd.x = oneThirdEndX
  measurementData.handles.perpendicularEnd.y = oneThirdEndY
  measurementData.handles.lastPerpendicularStart.x = oneThirdStartX
  measurementData.handles.lastPerpendicularStart.y = twoThirdStartY
  measurementData.handles.lastPerpendicularEnd.x = oneThirdEndX
  measurementData.handles.lastPerpendicularEnd.y = twoThirdEndY

  return true
}
