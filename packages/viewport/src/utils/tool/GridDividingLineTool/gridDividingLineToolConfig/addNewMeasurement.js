import {
  importInternal,
  external,
  EVENTS,
  addToolState,
  removeToolState,
  getToolState,
} from 'cornerstone-tools'
import getActiveTool from '../../getActiveTool'
import GridDividingLineTool from '../GridDividingLineTool'
import updatePerpendicularLineHandles from './updatePerpendicularLineHandles'

const moveNewHandle = importInternal('manipulators/moveNewHandle')
const anyHandlesOutsideImage = importInternal('manipulators/anyHandlesOutsideImage')
const triggerEvent = importInternal('util/triggerEvent')

export default function (evt, interactionType) {
  const eventData = evt.detail
  const { element, image, buttons } = eventData

  const config = this.configuration

  if (checkPixelSpacing(image)) {
    return
  }

  const measurementData = this.createNewMeasurement(eventData)

  const doneCallback = () => {
    measurementData.active = false
    external.cornerstone.updateImage(element)
  }

  addToolState(element, this.name, measurementData)
  external.cornerstone.updateImage(element)

  const timestamp = new Date().getTime()
  const { end } = measurementData.handles

  moveNewHandle(eventData, this.name, measurementData, end, {}, interactionType, (success) => {
    const toolState = getToolState(element, this.name)?.data
    if (toolState && Array.isArray(toolState) && toolState.length > 1) {
      removeToolState(element, this.name, measurementData)
      return
    }
    if (!success) {
      removeToolState(element, this.name, measurementData)

      return
    }
    const { handles } = measurementData
    const hasHandlesOutside = anyHandlesOutsideImage(eventData, handles)
    const isTooFast = new Date().getTime() - timestamp < 150

    if (hasHandlesOutside || isTooFast) {
      // Delete the measurement
      measurementData.cancelled = true
      removeToolState(element, this.name, measurementData)
    } else {
      // Set lesionMeasurementData Session
      config.getMeasurementLocationCallback(measurementData, eventData, doneCallback)
    }

    // Update perpendicular line and disconnect it from the long-line
    updatePerpendicularLineHandles(eventData, measurementData)

    measurementData.invalidated = true

    external.cornerstone.updateImage(element)

    const activeTool = getActiveTool(element, buttons, interactionType)

    if (activeTool instanceof GridDividingLineTool) {
      activeTool.updateCachedStats(image, element, measurementData)
    }

    const modifiedEventData = {
      toolName: this.name,
      element,
      measurementData,
    }

    triggerEvent(element, EVENTS.MEASUREMENT_MODIFIED, modifiedEventData)
    triggerEvent(element, EVENTS.MEASUREMENT_COMPLETED, modifiedEventData)
  })
}

const checkPixelSpacing = (image) => {
  const imagePlane = external.cornerstone.metaData.get('imagePlaneModule', image.imageId)
  let rowPixelSpacing = image.rowPixelSpacing
  let colPixelSpacing = image.columnPixelSpacing

  if (imagePlane) {
    rowPixelSpacing = imagePlane.rowPixelSpacing || imagePlane.rowImagePixelSpacing
    colPixelSpacing = imagePlane.columnPixelSpacing || imagePlane.colImagePixelSpacing
  }

  // LT-29 Disable Target Measurements when pixel spacing is not available
  return !rowPixelSpacing || !colPixelSpacing
}
