import { importInternal } from 'cornerstone-tools'

const getLogger = importInternal('util/getLogger')

const logger = getLogger('tools:annotation:TestTool')
const lineSegDistance = importInternal('util/lineSegDistance')

/**
 *
 *
 * @param {*} element
 * @param {*} data
 * @param {*} coords
 * @returns {Boolean}
 */
export default function (element, data, coords) {
  const validParameters = data && data.handles && data.handles.start && data.handles.end

  if (!validParameters) {
    logger.warn(`invalid parameters supplied to tool ${this.name}'s pointNearTool`)

    return false
  }

  if (data.visible === false) {
    return false
  }

  return lineSegDistance(element, data.handles.start, data.handles.end, coords) < 25
}
