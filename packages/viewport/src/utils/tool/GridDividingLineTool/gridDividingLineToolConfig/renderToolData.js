import { getModule, getToolState, importInternal, toolColors } from 'cornerstone-tools'
import updatePerpendicularLineHandles from './updatePerpendicularLineHandles'

const getNewContext = importInternal('drawing/getNewContext')
const draw = importInternal('drawing/draw')
const setShadow = importInternal('drawing/setShadow')
const drawLine = importInternal('drawing/drawLine')
const drawHandles = importInternal('drawing/drawHandles')
export default function (evt) {
  const eventData = evt.detail
  const { handleRadius, drawHandlesOnHover, hideHandlesIfMoving, renderDashed } = this.configuration
  const toolData = getToolState(evt.currentTarget, this.name)

  if (!toolData) {
    return
  }

  // We have tool data for this element - iterate over each one and draw it
  const context = getNewContext(eventData.canvasContext.canvas)
  const { element } = eventData

  const lineDash = getModule('globalConfiguration').configuration.lineDash

  for (let i = 0; i < toolData.data.length; i++) {
    const data = toolData.data[i]

    if (data.visible === false) {
      continue
    }

    draw(context, (context) => {
      // Configurable shadow
      setShadow(context, this.configuration)

      const {
        start,
        end,
        perpendicularStart,
        perpendicularEnd,
        lastPerpendicularStart,
        lastPerpendicularEnd,
      } = data.handles

      const color = toolColors.getColorIfActive(data)

      const lineOptions = { color }

      if (renderDashed) {
        lineOptions.lineDash = lineDash
      }
      updatePerpendicularLineHandles(eventData, data)

      drawLine(context, element, start, end, lineOptions)

      drawLine(context, element, perpendicularStart, perpendicularEnd, lineOptions)

      drawLine(context, element, lastPerpendicularStart, lastPerpendicularEnd, lineOptions)

      // Draw the handles
      const handleOptions = {
        color,
        handleRadius,
        drawHandlesIfActive: drawHandlesOnHover,
        hideHandlesIfMoving,
      }

      if (this.configuration.drawHandles) {
        drawHandles(context, eventData, data.handles, handleOptions)
      }
    })
  }
}
