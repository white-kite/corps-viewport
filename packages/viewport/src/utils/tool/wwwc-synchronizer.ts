import {
  Synchronizer,
  wwwcSynchronizer,
  // @ts-ignore
} from 'cornerstone-tools'

export const useWwwcSynchronizer = () =>
  new Synchronizer('cornerstoneimagerendered', wwwcSynchronizer)
