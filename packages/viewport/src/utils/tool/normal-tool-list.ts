import { Tools } from '../../types'
import { GridDividingLineTool } from './GridDividingLineTool'
import { shallowRef } from 'vue'

export const useNormalCornerstoneToolList = () => {
  const toolNameList = [
    'Wwwc',
    'Pan',
    'Zoom',
    'Magnify',
    'Rotate',
    'CobbAngle',
    'Length',
    'Bidirectional',
    'Angle',
    'RectangleRoi',
    'Probe',
    'EllipticalRoi',
    'FreehandRoi',
  ]

  const getTools = (): Tools => {
    const tools: Tools = toolNameList.map((name) => ({
      name,
      mode: 'disabled',
      modeOptions: { mouseButtonMask: 1 },
    }))
    tools.push({
      name: 'GridDividingLine',
      toolClass: GridDividingLineTool,
      mode: 'disabled',
      modeOptions: { mouseButtonMask: 1 },
    })
    return tools
  }

  return shallowRef(getTools())
}
