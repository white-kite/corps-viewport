import {
  StackScrollMouseWheelTool,
  addStackStateManager,
  addToolState,
  addToolForElement,
  setToolActiveForElement,
  removeToolForElement,
  clearToolState,
  loadHandlerManager,
  stackPrefetch,
  // @ts-ignore
} from 'cornerstone-tools'
import { Ref, ref } from 'vue'
/**
 * 使用鼠标滚动多个dicom片
 */
export const useCornerstoneStackScrollMouseWheelTool = (
  isImageLoading: Ref<boolean>,
  isPrefetchLoading: Ref<boolean>
) => {
  /**
   * 增加滚动工具状态
   * @param element
   * @param stack
   */
  const addStackScrollMouseWheelToolState = (
    element: HTMLElement | undefined,
    stack: { currentImageIdIndex: number; imageIds: Array<string> }
  ) => {
    if (element) {
      clearToolState(element, 'stack')
      addStackStateManager(element, ['stack'])
      addToolState(element, 'stack', stack)
      if (stack.imageIds && stack.imageIds.length > 1) {
        isPrefetchLoading.value = true
        stackPrefetch.enable(element)
      }
    }
  }

  /**
   * 注册工具
   */
  const registerStackScrollMouseWheelToolTool = (element?: HTMLElement) => {
    addToolForElement(element, StackScrollMouseWheelTool)
    setToolActiveForElement(element, 'StackScrollMouseWheel', {})
  }
  /**
   * 卸载工具
   * @param element
   */
  const uninstallStackScrollMouseWheelToolTool = (element?: HTMLElement) => {
    removeToolForElement(element, 'StackScrollMouseWheel')
  }

  const loadHandlerTimeout = ref()

  const setupLoadHandlers = (element?: HTMLElement, clear = false) => {
    if (clear) {
      loadHandlerManager.removeHandlers(element)
      return
    }

    const startLoadHandler = () => {
      clearTimeout(loadHandlerTimeout.value)

      // We're taking too long. Indicate that we're "Loading".
      loadHandlerTimeout.value = setTimeout(() => {
        isImageLoading.value = true
      }, 100)
    }

    const endLoadHandler = () => {
      clearTimeout(loadHandlerTimeout.value)

      if (isImageLoading.value) {
        isImageLoading.value = false
      }
    }

    loadHandlerManager.setStartLoadHandler(startLoadHandler, element)
    loadHandlerManager.setEndLoadHandler(endLoadHandler, element)
  }

  return {
    addStackScrollMouseWheelToolState,
    registerStackScrollMouseWheelToolTool,
    uninstallStackScrollMouseWheelToolTool,
    setupLoadHandlers,
  }
}
