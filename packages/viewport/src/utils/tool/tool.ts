// @ts-ignore
import cornerstoneTools from 'cornerstone-tools'
import { Tools } from '../../types'
import getActiveToolsForElement from './getActiveToolsForElement'

const AVAILABLE_TOOL_MODES = ['active', 'passive', 'enabled', 'disabled']
const TOOL_MODE_FUNCTIONS = {
  active: cornerstoneTools.setToolActiveForElement,
  passive: cornerstoneTools.setToolPassiveForElement,
  enabled: cornerstoneTools.setToolEnabledForElement,
  disabled: cornerstoneTools.setToolDisabledForElement,
}

export const useCornerstoneTool = () => {
  const registerCornerstoneTool = (tools: Tools, element?: HTMLElement | null) => {
    if (element) {
      for (let i = 0; i < tools.length; i++) {
        const tool = Object.assign({}, tools[i])
        const toolName = `${tool.name}Tool`

        tool.toolClass = tool.toolClass || cornerstoneTools[toolName]

        if (!tool.toolClass) {
          console.warn(`Unable to add tool with name '${tool.name}'.`)
          continue
        }

        cornerstoneTools.addToolForElement(element, tool.toolClass, tool.props || {})

        const hasInitialMode = tool.mode && AVAILABLE_TOOL_MODES.includes(tool.mode)

        if (hasInitialMode) {
          const setToolModeFn = TOOL_MODE_FUNCTIONS[tool.mode]
          setToolModeFn(element, tool.name, tool.modeOptions || {})
        }
      }
    }
  }

  const disableCornerstoneToolWhileActiveToolNameEmpty = (element: HTMLElement, validTools) => {
    const state = cornerstoneTools.store.state
    const activeToolsForElement = getActiveToolsForElement(element, validTools)
    activeToolsForElement
      .filter((item) => item.name !== 'StackScrollMouseWheel')
      .forEach((item) => {
        cornerstoneTools.setToolDisabledForElement(element, item.name, {
          isMouseActive: false,
          isTouchActive: false,
        })
      })
    const svgCursorUrl = state.svgCursorUrl
    if (svgCursorUrl) {
      window.URL.revokeObjectURL(svgCursorUrl)
    }

    state.svgCursorUrl = null
    element.style.cursor = 'initial'
  }

  const activeCornerstoneTool = (
    element: HTMLElement | undefined,
    activeToolName: string,
    synchronizationContext?: {
      add: (item: HTMLElement) => void
      enabled?: boolean
    }
  ) => {
    if (!element) {
      return
    }
    const state = cornerstoneTools.store.state
    const validTools = state.tools.filter(
      (tool: { element: HTMLElement }) => tool.element === element
    )
    if (!activeToolName) {
      disableCornerstoneToolWhileActiveToolNameEmpty(element, validTools)
      return
    }
    if (synchronizationContext) {
      synchronizationContext.add(element)
    }
    const validToolNames = validTools.map((tool: { name: string }) => tool.name)

    if (!validToolNames.includes(activeToolName)) {
      console.warn(
        `Trying to set a tool active that is not "added". Available tools include: ${validToolNames.join(
          ', '
        )}`
      )
    }

    cornerstoneTools.setToolActiveForElement(
      element,
      activeToolName,
      synchronizationContext
        ? {
            mouseButtonMask: 1,
            synchronizationContext,
          }
        : {
            mouseButtonMask: 1,
          }
    )
  }
  return { registerCornerstoneTool, activeCornerstoneTool }
}
