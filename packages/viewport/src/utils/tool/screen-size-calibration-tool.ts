import { getImage, getViewport, setViewport } from 'cornerstone-core'

export const useCornerstoneScreenSizeCalibration = () => {
  return (element: HTMLElement | undefined, w: number) => {
    if (!element) {
      return
    }
    const image = getImage(element)
    if (!image) {
      return
    }
    const screenPixelWidth = screen.availWidth * devicePixelRatio
    const screenPixelSize = w / screenPixelWidth
    const percent = image.rowPixelSpacing / screenPixelSize
    const viewport = getViewport(element)
    viewport.scale = percent
    setViewport(element, viewport)
  }
}
