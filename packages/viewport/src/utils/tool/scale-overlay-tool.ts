import {
  ScaleOverlayTool,
  addToolForElement,
  setToolActiveForElement,
  removeToolForElement,
  // @ts-ignore
} from 'cornerstone-tools'

/**
 * 标尺工具
 */
export const useCornerstoneScaleOverlayTool = () => {
  /**
   * 注册工具
   */
  const registerScaleOverlayTool = (element?: HTMLElement) => {
    addToolForElement(element, ScaleOverlayTool)
    setToolActiveForElement(element, 'ScaleOverlay', {
      mouseButtonMask: 1,
    })
  }

  const uninstallScaleOverlayTool = (element?: HTMLElement) => {
    removeToolForElement(element, 'ScaleOverlay')
  }

  return { registerScaleOverlayTool, uninstallScaleOverlayTool }
}
