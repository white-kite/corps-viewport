// @ts-ignore
import { panZoomSynchronizer, Synchronizer } from 'cornerstone-tools'

export const usePanZoomSynchronizer = () => {
  return new Synchronizer('cornerstoneimagerendered', panZoomSynchronizer)
}
