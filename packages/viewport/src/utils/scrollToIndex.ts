import { importInternal } from 'cornerstone-tools'
const scrollToIndex = importInternal('util/scrollToIndex')
/**
 * 滚动至序列对应下标
 */
export const useCornerstoneScrollToIndex = () => {
  return (element: HTMLElement | undefined, newImageIdIndex: number) => {
    if (element) {
      scrollToIndex(element, newImageIdIndex)
    }
  }
}
