/**
 * formatNumberPrecision
 * @param numbers
 * @param precision
 * @copyright https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/helpers/formatNumberPrecision.js
 */
export const formatNumberPrecision = (numbers: number, precision: number) => {
  if (numbers !== null) {
    return Number(parseFloat(String(numbers)).toFixed(precision))
  }
}
