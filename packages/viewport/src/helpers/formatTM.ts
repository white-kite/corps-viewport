import { toDayJs, parseDateString } from '@vill-v/date'

/**
 * 格式化时间
 * @param time
 * @param strFormat
 * @copyright https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/helpers/formatTM.js
 */
export const formatTM = (time: string, strFormat = 'HH:mm:ss') => {
  if (!time) {
    return
  }

  // DICOM Time is stored as HHmmss.SSS, where:
  //      HH 24 hour time:
  //      m mm    0..59   Minutes
  //      s ss    0..59   Seconds
  //      S SS SSS    0..999  Fractional seconds
  //
  // Goal: '24:12:12'
  try {
    const strTime = time

    const parsedTimeSecond = toDayJs(strTime, 'HHmmss')

    if (parsedTimeSecond.isValid()) {
      return parseDateString(parsedTimeSecond, strFormat)
    }

    const parseTimeMillisecond = toDayJs(strTime, 'HHmmss.SSS')
    if (parseTimeMillisecond.isValid()) {
      return parseDateString(parseTimeMillisecond, strFormat)
    }

    return ''
  } catch (err) {
    // swallow?
  }

  return
}
