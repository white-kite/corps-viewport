import { textDecode, isStringEmpty, isObjectEmpty } from '@vill-v/vanilla'
import { isUndefined } from '@vill-v/type-as'

/**
 * 格式化基石标签
 */
export const formatTag = (
  specificCharacterSetAttribute: string,
  byteArray: Uint8Array,
  element: { dataOffset: number; length: number }
) => {
  /**
   * 获取基石是否拥有特殊编码格式
   * @param charset
   */
  const getCharset = (charset?: string) => {
    if (!charset || isStringEmpty(charset)) {
      return 'GB2312'
    }
    return ['GB18030', 'GB2312', 'GBK'].includes(charset) ? charset : 'GB2312'
  }
  if (!byteArray || !byteArray.length) {
    return ''
  }
  if (
    !element ||
    isObjectEmpty(element) ||
    isUndefined(element.length) ||
    isUndefined(element.dataOffset)
  ) {
    return ''
  }
  const { dataOffset, length } = element
  const subByteArray = byteArray.subarray(dataOffset, dataOffset + length)
  if (!subByteArray || !subByteArray.length) {
    return ''
  }
  return textDecode(subByteArray, getCharset(specificCharacterSetAttribute) || 'GB2312')
}
