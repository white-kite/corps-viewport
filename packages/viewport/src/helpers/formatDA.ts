import { toDayJs, parseDateString } from '@vill-v/date'

/**
 * 格式化时间
 * @param date
 * @param strFormat
 * @copyright https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/helpers/formatDA.js
 */
export const formatDA = (date?: string, strFormat = 'YYYY-MM-DD') => {
  if (!date) {
    return
  }
  // Goal: 'Apr 5, 1999'
  try {
    const parsedDateTime = toDayJs(date, 'YYYYMMDD')
    return parsedDateTime.isValid() ? parseDateString(parsedDateTime, strFormat) : ''
  } catch (err) {
    // swallow?
  }

  return
}
