/**
 * Formats a patient name for display purposes
 * @param name
 * @copyright https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/helpers/formatPN.js
 */
export const formatPN = (name: string) => {
  if (!name) {
    return
  }

  // Convert the first ^ to a ', '. String.replace() only affects
  // the first appearance of the character.
  const commaBetweenFirstAndLast = name.replace('^', ', ')

  // Replace any remaining '^' characters with spaces
  const cleaned = commaBetweenFirstAndLast.replace(/\^/g, ' ')

  // Trim any extraneous whitespace
  return cleaned.trim()
}
