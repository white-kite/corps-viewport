/**
 * 检查是否为number类型
 * @param value
 * @copyright https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/helpers/isValidNumber.js
 */
export const isValidNumber = (value: unknown) => {
  return typeof value === 'number' && !isNaN(value)
}
