import { ViewportOverlayType } from '../ViewportOverlay'

export const GetOverlay = ({
  Component,
  imageIds,
  imageIdIndex,
  windowWidth,
  isOverlayVisible,
  scale,
  windowCenter,
  hideSensitiveInformation,
}: {
  Component: ViewportOverlayType
  imageIds: Array<string>
  imageIdIndex: number
  windowWidth: number
  isOverlayVisible: boolean
  scale: number
  windowCenter: number
  hideSensitiveInformation: boolean
}) => {
  const imageId = imageIds[imageIdIndex]
  return imageId && windowWidth && isOverlayVisible ? (
    <Component
      hideSensitiveInformation={hideSensitiveInformation}
      imageIndex={imageIdIndex + 1}
      stackSize={imageIds.length}
      scale={scale}
      windowWidth={windowWidth}
      windowCenter={windowCenter}
      imageId={imageId}
    />
  ) : null
}
