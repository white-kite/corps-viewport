import '../metadataProvider'
import {
  computed,
  defineComponent,
  toRef,
  watch,
  onBeforeUnmount,
  onMounted,
  watchEffect,
} from 'vue'
import { GetLoadingIndicator } from './getLoadingIndicator'
import { LoadingIndicator } from '../LoadingIndicator'
import { GetOverlay } from './getOverlay'
import { ViewportOverlay } from '../ViewportOverlay'
import { GetOrientationMarkersOverlay } from './getOrientationMarkersOverlay'
import { ImageScrollbar } from '../ImageScrollbar'
import {
  useCornerstone,
  useCornerstoneLoadImage,
  useCornerstoneReset,
  useCornerstoneResize,
  useCornerstoneScaleOverlayTool,
  useCornerstoneStackPrefetch,
  useCornerstoneStackScrollMouseWheelTool,
  useCornerstoneTool,
  useCornerstoneScrollToIndex,
} from '../utils'

import { areStringArraysEqual } from '../helpers'
import { useElementSize, useResizeObserver } from '@vueuse/core'
import styles from './index.module.scss'
import { any, array, arrayOf, bool, func, object, oneOf, shape, string } from 'vue-types'

import { Tool, Tools } from '../types'
import { isArrayEmpty } from '@vill-v/vanilla'

export type { Tool, Tools }
export type ToolItem =
  | {
      name: string
      mode: 'active' | 'passive' | 'enabled' | 'disabled'
      modeOptions?: { mouseButtonMask: number }
    }
  | string
export type ToolList = Array<ToolItem>

export const CornerstoneViewport = defineComponent({
  name: 'CornerstoneViewport',
  props: {
    activeTool: string(),
    synchronizationContext: object(),
    tools: arrayOf(
      shape<Tool>({
        name: string(),
        toolClass: func(),
        props: object(),
        mode: oneOf<('active' | 'passive' | 'enabled' | 'disabled')[]>([
          'active',
          'passive',
          'enabled',
          'disabled',
        ]),
        modeOptions: object<{ mouseButtonMask: number } | Record<string, number>>(),
      }).loose
    ).def([]),
    imageIds: array<string>().isRequired.def([]),
    hideSensitiveInformation: bool().def(false),
    style: any(),
    class: string(),
  },
  emits: ['setViewportActive', 'viewportInit', 'imageLoad', 'imagePrefetch'],
  setup: (props, { emit, expose }) => {
    /**
     * 图像id列表
     */
    const imageIds = toRef(props, 'imageIds')

    /**
     * customStyle
     */
    const style = toRef(props, 'style')

    const hideSensitiveInformation = toRef(props, 'hideSensitiveInformation')

    /**
     * 基石的基础操作
     */
    const { viewRef, enable, disable } = useCornerstone()

    /**
     * 基石图像重新计算大小
     */
    const cornerstoneResize = useCornerstoneResize()

    /**
     * 基石图像查看器重置
     */
    const cornerstoneReset = useCornerstoneReset()

    const { registerCornerstoneTool, activeCornerstoneTool } = useCornerstoneTool()

    /**
     * 基石图像操作
     */
    const {
      loadAndCacheImage,
      displayImage,
      bindInternalCornerstoneEventListeners,
      imageProgress,
      isImageLoading,
      error,
      imageIdIndex,
      bindInternalElementEventListeners,
      scale,
      windowCenter,
      windowWidth,
      rotationDegrees,
      isFlippedVertically,
      isFlippedHorizontally,
    } = useCornerstoneLoadImage(imageIds, emit)

    const isPrefetchLoading = useCornerstoneStackPrefetch(viewRef, error)

    /**
     * 基石滚动切换图片
     */
    const {
      addStackScrollMouseWheelToolState,
      registerStackScrollMouseWheelToolTool,
      uninstallStackScrollMouseWheelToolTool,
      setupLoadHandlers,
    } = useCornerstoneStackScrollMouseWheelTool(isImageLoading, isPrefetchLoading)

    /**
     * 基石标尺工具
     */
    const { registerScaleOverlayTool, uninstallScaleOverlayTool } = useCornerstoneScaleOverlayTool()

    const scrollToIndex = useCornerstoneScrollToIndex()

    /**
     * 图片切换滚动条改变回调
     * @param value
     */
    const imageSliderOnInputCallback = (value: number) => {
      imageIdIndex.value = value
      scrollToIndex(viewRef.value, value)
    }

    /**
     *  高度
     */
    const { height } = useElementSize(viewRef)

    /**
     * 计算滚动条高度
     */
    const scrollbarHeight = computed(() => (height.value ? `${height.value - 20}px` : '100px'))

    /**
     * 计算滚动条滚动最大限制
     */
    const scrollbarMax = computed(() => imageIds.value.length - 1)

    /**
     * 加载图片
     * @param newImageIds
     */
    const loadImage = (newImageIds: Array<string>) => {
      if (!newImageIds || isArrayEmpty(newImageIds)) {
        return
      }
      loadAndCacheImage(newImageIds[0]).then((image) => {
        cornerstoneResize(viewRef.value, true)
        error.value || displayImage(viewRef.value, image)
        cornerstoneReset(viewRef.value)
        addStackScrollMouseWheelToolState(viewRef.value, {
          imageIds: newImageIds,
          currentImageIdIndex: imageIdIndex.value,
        })
      })
    }
    /**
     * 监听imageIds，改变时重新渲染图片
     */
    watch(imageIds, (newImageIds, oldImageIds) => {
      if (!areStringArraysEqual(newImageIds, oldImageIds || [])) {
        if (newImageIds && !isArrayEmpty(newImageIds)) {
          loadImage(newImageIds)
        }
      }
    })

    watch(
      isImageLoading,
      (newImageLoading) => {
        emit('imageLoad', newImageLoading)
      },
      { immediate: true }
    )

    watch(
      isPrefetchLoading,
      (newPrefetchLoading) => {
        emit('imagePrefetch', newPrefetchLoading)
      },
      { immediate: true }
    )

    watchEffect(() => {
      activeCornerstoneTool(
        viewRef.value,
        props.activeTool as string,
        props.synchronizationContext as {
          add: (item: HTMLElement) => void
          enabled?: boolean
        }
      )
    })

    expose({
      element: viewRef,
      imageIdIndex,
      isPrefetchLoading,
    })

    useResizeObserver(viewRef, () => cornerstoneResize(viewRef.value, true))

    const install = () => {
      enable(viewRef.value)
      loadImage(imageIds.value)
      bindInternalCornerstoneEventListeners()
      bindInternalElementEventListeners(viewRef.value)
      setupLoadHandlers(viewRef.value)
      registerStackScrollMouseWheelToolTool(viewRef.value)
      registerScaleOverlayTool(viewRef.value)
      registerCornerstoneTool(props.tools as Tools, viewRef.value)
      emit('viewportInit', viewRef.value)
    }
    onMounted(() => {
      install()
    })

    const uninstall = () => {
      bindInternalCornerstoneEventListeners(true)
      bindInternalElementEventListeners(viewRef.value, true)
      setupLoadHandlers(viewRef.value, true)
      uninstallStackScrollMouseWheelToolTool(viewRef.value)
      uninstallScaleOverlayTool(viewRef.value)
      disable(viewRef.value)
    }

    onBeforeUnmount(() => {
      uninstall()
    })
    return () => (
      <div class={[styles.viewportWrapper, props.class]} style={style.value}>
        <div
          ref={viewRef}
          class={styles.viewportElement}
          onContextmenu={(e) => e.preventDefault()}
          onMousedown={(e) => e.preventDefault()}
        >
          {(isImageLoading.value || error.value) && (
            <GetLoadingIndicator
              Component={LoadingIndicator}
              error={error.value}
              imageProgress={imageProgress.value}
              isPrefetchLoading={isPrefetchLoading.value}
            />
          )}
          <canvas class="cornerstone-canvas" />
          <GetOverlay
            hideSensitiveInformation={hideSensitiveInformation.value}
            Component={ViewportOverlay}
            imageIds={imageIds.value}
            imageIdIndex={imageIdIndex.value}
            windowWidth={windowWidth.value}
            isOverlayVisible
            scale={scale.value}
            windowCenter={windowCenter.value}
          />
          <GetOrientationMarkersOverlay
            imageIds={imageIds.value}
            imageIdIndex={imageIdIndex.value}
            isFlippedVertically={isFlippedVertically.value}
            isFlippedHorizontally={isFlippedHorizontally.value}
            rotationDegrees={rotationDegrees.value}
          />
        </div>
        <ImageScrollbar
          value={imageIdIndex.value}
          max={scrollbarMax.value}
          height={scrollbarHeight.value}
          onInputCallback={imageSliderOnInputCallback}
        />
      </div>
    )
  },
})
