import { ViewportOrientationMarkers } from '../ViewportOrientationMarkers'
import { metaData } from 'cornerstone-core'

export const GetOrientationMarkersOverlay = ({
  imageIds,
  imageIdIndex,
  rotationDegrees,
  isFlippedVertically,
  isFlippedHorizontally,
}: {
  imageIds: Array<string>
  imageIdIndex: number
  rotationDegrees?: number
  isFlippedVertically: boolean
  isFlippedHorizontally: boolean
}) => {
  const imageId = imageIds[imageIdIndex]

  if (!imageId) {
    return null
  }
  const { rowCosines, columnCosines } = metaData.get('imagePlaneModule', imageId) || {}

  if (!rowCosines || !columnCosines || rotationDegrees === undefined) {
    return null
  }

  return (
    <ViewportOrientationMarkers
      rowCosines={rowCosines}
      columnCosines={columnCosines}
      rotationDegrees={rotationDegrees}
      isFlippedVertically={isFlippedVertically}
      isFlippedHorizontally={isFlippedHorizontally}
    />
  )
}
