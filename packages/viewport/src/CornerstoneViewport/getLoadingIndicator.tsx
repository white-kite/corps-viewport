import { LoadingIndicatorType } from '../LoadingIndicator'

/**
 * 获取加载指示器
 * @param Component
 * @param error
 * @param imageProgress
 * @param isPrefetchLoading
 * @constructor
 */
export const GetLoadingIndicator = ({
  Component,
  error,
  imageProgress,
  isPrefetchLoading,
}: {
  Component: LoadingIndicatorType
  error: { message: string } | null
  imageProgress: number
  isPrefetchLoading: boolean
}) => (
  <Component error={error} percentComplete={imageProgress} isPrefetchLoading={isPrefetchLoading} />
)
