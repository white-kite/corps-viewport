export type Tool = {
  name: string
  toolClass?: unknown
  props?: Record<string, string>
  mode: 'active' | 'passive' | 'enabled' | 'disabled'
  modeOptions: { mouseButtonMask: number } | Record<string, number>
}

export type Tools = Array<Tool>
