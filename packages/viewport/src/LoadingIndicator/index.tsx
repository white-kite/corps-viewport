import { FunctionalComponent } from 'vue'
import { LoadingOutlined } from '@ant-design/icons-vue'
import styles from './index.module.scss'

export interface LoadingIndicatorProps {
  percentComplete: number
  error: { message: string } | null
  isPrefetchLoading: boolean
}

export type LoadingIndicatorType = FunctionalComponent<LoadingIndicatorProps>

/**
 * 加载中指示器
 * @param percentComplete
 * @param error
 * @param isPrefetchLoading
 * @constructor
 * @copyright  https://github.com/cornerstonejs/react-cornerstone-viewport/blob/master/src/LoadingIndicator/LoadingIndicator.js
 */
export const LoadingIndicator: LoadingIndicatorType = ({
  percentComplete = 0,
  error = null,
  isPrefetchLoading,
}) => (
  <>
    {error ? (
      <div class={[styles.imageViewerErrorLoadingIndicator, styles.loadingIndicator]}>
        <div class={styles.indicatorContents}>
          <h4>影像加载失败</h4>
          <p class={styles.description}>发生了未知的错误</p>
          <p class={styles.details}>{error.message}</p>
        </div>
      </div>
    ) : (
      <div class={`${styles.imageViewerLoadingIndicator} ${styles.loadingIndicator}`}>
        <div class={styles.indicatorContents}>
          {(percentComplete < 100 || isPrefetchLoading) && (
            <LoadingOutlined style={{ color: '#fff', fontSize: '30px' }} />
          )}
          <h4 style={{ color: '#fff' }}>
            {percentComplete < 100 || isPrefetchLoading ? '加载中' : '加载完成'}
          </h4>
        </div>
      </div>
    )}
  </>
)
