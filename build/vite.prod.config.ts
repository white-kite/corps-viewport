import { defineConfig, mergeConfig } from 'vite'
import { resolve } from 'node:path'
import { baseConfig } from './vite.base.config'

const FormatExt = {
  es: '.mjs',
  cjs: '.js',
}

export const prodConfig = mergeConfig(
  baseConfig,
  defineConfig({
    server: {
      host: true,
    },
    build: {
      lib: {
        entry: resolve(__dirname, 'src/index.ts'),
        name: 'name',
        formats: ['cjs', 'es'],
        fileName: (m) => `index${FormatExt[m]}`,
      },
      rollupOptions: {
        external: [
          'vue',
          'hammerjs',
          'dicom-parser',
          'vue-types',
          '@vueuse/core',
          '@ant-design/icons-vue',
          RegExp(String.raw`@vill-v/.*`),
          RegExp(String.raw`cornerstone-.*`),
        ],
      },
      minify: false,
      target: 'es2015',
    },
  })
)
