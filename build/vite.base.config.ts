import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import { resolve } from 'node:path'
import { kebabCase } from 'scule'
export const baseConfig = defineConfig({
  server: {
    host: true,
  },
  plugins: [vue(), vueJsx()],
  css: {
    modules: {
      localsConvention: 'camelCaseOnly',
      generateScopedName: (name) => 'wk-corps-viewport-' + kebabCase(name),
    },
  },
  resolve: {
    alias: {
      '@whitekite/corps-viewport': resolve(__dirname, '../packages/viewport/src/index.ts'),
    },
  },
})
